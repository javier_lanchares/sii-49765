/////////logger.cpp
/////////////////////////////
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <fstream>
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <errno.h>

using namespace std;
///////////////////////////////////////

int main(void)
{
	int fd,p1=0,p2=0;
	char buf[1];

	mkfifo("conteopuntos",0777);

	fd=open("conteopuntos",O_RDONLY);
	

	while(1)
	{
		read(fd,buf,sizeof(buf));

		if(buf[0]==49)
		{
			p1++;
			printf("El jugador 1 ha marcado un punto, lleva un total de %d \n",p1);
		}
		
		else
		{
			p2++;
			printf("El jugador 2 ha marcado un punto, lleva un total de %d \n",p2);
		}

	}

	close(fd);
	unlink("conteopuntos");

	return 0;
}
