#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <math.h>

#include "DatosMemCompartida.h"

int main(){

	DatosMemCompartida * dbot;
	int fd, acc;

	fd = open("/tmp/pro_mem", O_CREAT^O_RDWR, 0777);
	
	ftruncate(fd, sizeof(*dbot));

	dbot = (DatosMemCompartida *)mmap(NULL, sizeof(*dbot), PROT_WRITE^PROT_READ, MAP_SHARED, fd, 0);


	while(1)
	{	
	
		if(dbot->esfera.centro.y > dbot->raqueta1.y1) acc = 1;
		else if(dbot->esfera.centro.y < dbot->raqueta1.y1) acc = -1;
		else acc = 0;
		dbot->accion = acc;
		
	
		usleep(25000);
	}
}
